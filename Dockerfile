#Deriving the latest base image
FROM python:buster

## 𝐶𝑜𝑛𝑓𝑖𝑔

###Convert to ENV Variables
ENV AWS_ACCESS_KEY_ID 		${AWS_ACCESS_KEY_ID} 
ENV AWS_SECRET_ACCESS_KEY	${AWS_SECRET_ACCESS_KEY}
ENV s3_bucket				${s3_bucket}
ENV DB_NAME					${DB_NAME}
ENV DB_USER					${DB_USER}
ENV DB_PASSWORD				${DB_PASSWORD}
ENV host                    ${HOST}


#Labels as key value pair
LABEL Maintainer="bimal"

RUN apt-get update && apt-get install -y mariadb-client


WORKDIR /usr/app/src

#to COPY the remote file at working directory in container
COPY ./ ./
# Now the structure looks like this '/usr/app/src/shub.py'

RUN pip install -r  requirements.txt

#CMD instruction should be used to run the software
#contained by your image, along with any arguments.

CMD [ "python", "./shub.py"]
